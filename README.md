# l1 calo ntuples

- setupATLAS
- lsetup git
- git clone ssh://git@gitlab.cern.ch:7999/pbellos/l1-calo-ntuples.git
- cd l1-calo-ntuples/
- mkdir build run
- cd build
- asetup 22.0.70, Athena
- cmake -DATLAS_PACKAGE_FILTER_FILE=../package_filters.txt ../athena/Projects/WorkDir
- make
- source x86_64-centos7-gcc11-opt/setup.sh
- cd ../run/
- python ../athena/Calib/share/physicsRunL1Calo.py
