from AthenaCommon.AppMgr import theApp
from AthenaCommon.AppMgr import ToolSvc
## get a handle on the ServiceManager
from AthenaCommon.AppMgr import ServiceMgr
import os,sys
#--------------------------------------------------------------
# Load POOL support
#--------------------------------------------------------------
import AthenaPoolCnvSvc.ReadAthenaPool
 
from glob import glob

if not "InputFiles" in dir():
    ServiceMgr.EventSelector.InputCollections = ["/data/silo01/users/fcastill/xAOD_pulser/xAOD_00405135.pool.root"]

# Reading runNumber
for f in ServiceMgr.EventSelector.InputCollections:
    runNumber=os.path.basename(f).split("_")[1].split(".")[0]
    if not runNumber.isnumeric():
        runNumber="NONE"



theApp.EvtMax=-1

# Set some output limits
ServiceMgr.MessageSvc.infoLimit = 100000
ServiceMgr.MessageSvc.errorLimit = 10
ServiceMgr.MessageSvc.OutputLevel = INFO
# TrigDecisionTool configuration
from TrigAnalysisExamples import TDTAthAnalysisConfig
#########################################################################
#                                                                       #
#                      Now set up the example job                       #
#                                                                       #
#########################################################################
# Define sets of triggers to use
#L1List= [ "L1_EM3_EMPTY", "L1_EM22VH", "L1_EM22VHI", "L1_EM24VHI", "L1_EM22VHI", "L1_EM24VHI", "L1_EM8VH", "L1_EM10VH", "L1_EM15", "L1_EM15VHI" ]
L1List= []

# Trigger Analysis 
# Add the examples to the top algorithm sequence
from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()
topSequence += CfgMgr.TriggerAnalysis( "TriggerAnalysis")
topSequence += CfgMgr.xAODreaderAlg()
#topSequence += CfgMgr.CaloCellAlg()
# For more info uncomment the line below 
#topSequence += CfgMgr.Trig__TDTExample( "TDTExample",         TriggerList=L1List)


# Histogram routing
ServiceMgr += CfgMgr.THistSvc()
outputNtuple = "CalibTrigDec_" + str(runNumber) + ".root"
ServiceMgr.THistSvc.Output  = ["Trigger DATAFILE='"+outputNtuple+"' OPT='RECREATE'"]
ServiceMgr.THistSvc.OutputLevel = DEBUG
