import os
from glob import glob
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.AllConfigFlags import ConfigFlags as flags
from AthenaConfiguration.MainServicesConfig import MainServicesCfg


flags.Input.Files =  glob("/eos/atlas/atlastier0/rucio/data21_900GeV/physics_Main/00405543/data21_900GeV.00405543.physics_Main.merge.DAOD_L1CALO2.f1196_m2077/data21_900GeV.00405543.physics_Main.merge.DAOD_L1CALO2.f1196_m2077._lb1841*")


flags.Exec.MaxEvents = -1
flags.lock()

#Reading runNumber
for f in flags.Input.Files:
    runNumber=os.path.basename(f).split(".")[1]
    if not runNumber.isnumeric():
        runNumber="NONE"

        
acc = MainServicesCfg(flags)
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
acc.merge(PoolReadCfg(flags))



from TrigDecisionTool.TrigDecisionToolConfig import TrigDecisionToolCfg
tdt = acc.getPrimaryAndMerge(TrigDecisionToolCfg(flags))



TriggerAnalysis = CompFactory.TriggerAnalysis( "TriggerAnalysis")
acc.addEventAlgo(TriggerAnalysis)
TriggerTowers = CompFactory.xAODreaderAlg()
acc.addEventAlgo(TriggerTowers)

# output streams for histograms
histSvc = CompFactory.THistSvc()
outputNtuple = "CalibTrigDec_" + str(runNumber) + ".root"
histSvc.Output  = ["Trigger DATAFILE='"+outputNtuple+"' OPT='RECREATE'"]



acc.addService(histSvc)

acc.run()

