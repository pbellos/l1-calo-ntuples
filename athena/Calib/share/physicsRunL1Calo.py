import glob, os,sys 
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.AllConfigFlags import ConfigFlags as flags
from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'




def LArDigits2NtupleCfg(flags):
    
    acc=ComponentAccumulator()
    from LArCabling.LArCablingConfig import LArOnOffIdMappingSCCfg, LArCalibIdMappingSCCfg, LArLATOMEMappingCfg
    acc.merge( LArOnOffIdMappingSCCfg(flags))
    #acc.merge( LArCalibIdMappingSCCfg(flags))
    acc.merge( LArLATOMEMappingCfg(flags))
    #fix for SC calib line mapping:
    from LArCabling.LArCablingConfig import _larCablingCfg
    LArCalibLineMappingAlg=CompFactory.getComps("LArCalibLineMappingAlg")
    acc.merge(_larCablingCfg(flags,LArCalibLineMappingAlg[0],"/LAR/Identifier/CalibIdMap_SC","LArCalibLineMappingAlgSC"))
    acc.getCondAlgo("LArCalibLineMappingAlgSC").WriteKey="LArCalibIdMapSC"
    acc.getCondAlgo("LArCalibLineMappingAlgSC").isSuperCell=True
    acc.getCondAlgo("LArCalibLineMappingAlgSC").MaxCL=16
    # and need also SC CaloDetDescr:
    acc.addCondAlgo(CompFactory.CaloSuperCellAlignCondAlg())
    
    SuperCells = True
    if not 'GainList' in dir():
         if SuperCells:
             GainList = ["SC"]
         else:
             GainList = [ "HIGH", "MEDIUM", "LOW" ]
            

            
    latome = CompFactory.LArDigits2NtupleEB()
    latome.AddFEBTempInfo=False
    latome.isSC = SuperCells
    
    latome.FillBCID = True
    latome.RealGeometry = True
    latome.OffId = True
    from AthenaCommon.Constants import DEBUG, INFO
    latome.OutputLevel = INFO

    acc.addEventAlgo(latome, 'AthAlgSeq')   

    return acc



def TriggerTowersNtupleCfg(flags):
    
    
    
    acc=ComponentAccumulator()
    
    from TrigT1CaloByteStream.LVL1CaloRun2ByteStreamConfig import LVL1CaloRun2ReadBSCfg
    acc.merge( LVL1CaloRun2ReadBSCfg(flags, forRoIBResultToxAOD=True))
    from CaloConditions.CaloConditionsConfig import CaloTriggerTowerCfg
    acc.merge( CaloTriggerTowerCfg(flags))




    decorator = CompFactory.LVL1.L1CaloTriggerTowerDecoratorAlg()
    decorator.TriggerTowerTools = CompFactory.LVL1.L1CaloxAODOfflineTriggerTowerTools()
    decorator.DecorName_caloCellEnergy = "CaloCellEnergy"
    decorator.DecorName_caloCellET = "CaloCellET"
    decorator.DecorName_caloCellEnergyByLayer = "CaloCellEnergyByLayer"
    decorator.DecorName_caloCellETByLayer = "CaloCellETByLayer"
    decorator.DecorName_caloCellEnergyByLayerByReceiver = "CaloCellEnergyByLayerByReceiver"
    decorator.DecorName_caloCellETByLayerByReceiver = "CaloCellETByLayerByReceiver"

    acc.addEventAlgo(decorator, 'AthAlgSeq')
    
    triggertowers = CompFactory.TriggerTowersNtuple()
    triggertowers.slices = 11  
    acc.addEventAlgo(triggertowers, 'AthAlgSeq')
    


    return acc



def L1CaloMenuCfg(flags, ListL1):
    
    acc=ComponentAccumulator()
    from TrigConfigSvc.TrigConfigSvcCfg import L1ConfigSvcCfg, HLTConfigSvcCfg, L1PrescaleCondAlgCfg, HLTPrescaleCondAlgCfg
   
    acc.merge( L1ConfigSvcCfg(flags) )
    acc.merge( HLTConfigSvcCfg(flags) )
    acc.merge( L1PrescaleCondAlgCfg(flags) )
    acc.merge( HLTPrescaleCondAlgCfg(flags) )

    
   # from TrigConfigSvc.TrigConfigSvcCfg import BunchGroupCondAlgCfg
   # acc.merge( BunchGroupCondAlgCfg( flags ) )

    tdm = CompFactory.getComp('TrigDec::TrigDecisionMakerMT')()
    tdm.doL1 = True
    tdm.doHLT = False
    acc.addEventAlgo( tdm, 'AthAlgSeq' )

    menuwriter = CompFactory.getComp("TrigConf::xAODMenuWriterMT")()
    menuwriter.KeyWriterTool = CompFactory.getComp('TrigConf::KeyWriterTool')()
    acc.addEventAlgo( menuwriter, 'AthAlgSeq' )
    
    from TrigDecisionTool.TrigDecisionToolConfig import TrigDecisionToolCfg
    tdt = acc.getPrimaryAndMerge(TrigDecisionToolCfg(flags))
    
    

    triggerMenuTuple = CompFactory.TriggerAnalysisNtuple(TriggerList=ListL1, TrigDecisionTool=tdt)
    acc.addEventAlgo(triggerMenuTuple) 

    

    return acc




# ===============================================================
#  main()
# ===============================================================
def main():
    from optparse import OptionParser
    parser = OptionParser(usage = "usage: %prog arguments", version="%prog")
    parser.add_option("-i","--InputFile",        dest="InputFile",                      help="Input raw data (default: %default)")
    parser.add_option("-f","--DirInputFiles",    dest="DirInputFiles",              help="Input directory of raw data (default: %default)")
    parser.add_option("-n","--ntuple",           dest="ntuple",   action="store_true",  help="option to do ntuple (default: %default)")
    ##parser.set_defaults(InputFile=["/eos/atlas/atlastier0/rucio/data22_900GeV/physics_Main/00425524/data22_900GeV.00425524.physics_Main.daq.RAW/data22_900GeV.00425524.physics_Main.daq.RAW._lb0125._SFO-12._0001.data"],ntuple=True,DirInputFiles="")
    ##parser.set_defaults(InputFile=["/eos/atlas/atlastier0/rucio/data22_13p6TeV/physics_Main/00427877/data22_13p6TeV.00427877.physics_Main.daq.RAW/data22_13p6TeV.00427877.physics_Main.daq.RAW._lb0720._SFO-16._0001.data"],ntuple=True,DirInputFiles="")
    parser.set_defaults(InputFile=["/eos/atlas/atlastier0/rucio/data22_13p6TeV/physics_Main/00432180/data22_13p6TeV.00432180.physics_Main.daq.RAW/data22_13p6TeV.00432180.physics_Main.daq.RAW._lb1180._SFO-20._0001.data"],ntuple=True,DirInputFiles="")
                                    
    
    (options,args) = parser.parse_args()
    
    import time
    from datetime import datetime
    start = time.time()
    print( "Starting Processing at : " + bcolors.OKBLUE + datetime.now().strftime('%H:%M:%S') + bcolors.ENDC)

    TriggerMenu = True
    
    print ("====================================================================================================================")
    
    
    ##################################################
    # Configure all the flags
    ##################################################

    print (options.InputFile)
    
    if options.DirInputFiles:
        flags.Input.Files = glob.glob(options.DirInputFiles)
    else:
        flags.Input.Files =  options.InputFile

        
    print ("Input file(s): ",flags.Input.Files)
    
    
    #Reading runNumber
    for f in flags.Input.Files:
        runNumber=os.path.basename(f).split(".")[1]
        if not runNumber.isnumeric():
            runNumber="NONE"
        

    flags.Exec.MaxEvents = 10
    flags.GeoModel.AtlasVersion = "ATLAS-R2-2016-01-00-01"
    flags.IOVDb.GlobalTag = "CONDBR2-BLKPA-RUN2-09"
    
    print ("------------------------1 ")
    
    

    if TriggerMenu:
        flags.Trigger.triggerConfig = 'DB'
        flags.Trigger.L1.doCTP = True
        flags.Trigger.L1.doMuon = False
        flags.Trigger.L1.doCalo = False
        flags.Trigger.L1.doTopo = False
    
    flags.Trigger.enableL1CaloLegacy = True
    flags.Trigger.enableL1CaloPhase1 = True

    print ("------------------------2 ")
    
    flags.lock()
    
    from AthenaConfiguration.MainServicesConfig import MainServicesCfg
    from ByteStreamCnvSvc.ByteStreamConfig import ByteStreamReadCfg
    from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg
    from AthenaConfiguration.ComponentFactory import CompFactory
    from TrigT1ResultByteStream.TrigT1ResultByteStreamConfig import L1TriggerByteStreamDecoderCfg
    from LArByteStream.LArRawSCDataReadingConfig import LArRawSCDataReadingCfg
    from CaloRec.CaloRecoConfig import CaloRecoCfg
    
    acc = MainServicesCfg(flags)
    acc.merge( ByteStreamReadCfg(flags) )
    acc.merge( L1TriggerByteStreamDecoderCfg(flags) )
    acc.merge( LArRawSCDataReadingCfg(flags) )
    acc.merge( CaloRecoCfg(flags))
    
    from TrigConfigSvc.TrigConfigSvcCfg import BunchGroupCondAlgCfg
    acc.merge( BunchGroupCondAlgCfg( flags ) )
    
    # Creating Trigger Tower and Latome ntuple
    #acc.merge(TriggerTowersNtupleCfg(flags))
    acc.merge(LArDigits2NtupleCfg(flags))
    
    print ("------------------------3 ")

    if TriggerMenu:
        # Adding trigger menu
        # Only three options can be analysed 
        L1List= [ "L1_EM3", "L1_EM15", "L1_EM7" ]
        acc.merge(L1CaloMenuCfg(flags, L1List))
        
    print ("------------------------4 ")

    if options.ntuple == True:
        OutputRootFileName =  "calib_" + str(runNumber) + ".root"
        ntupleSvc = CompFactory.NTupleSvc( Output =  [ "FILE1 DATAFILE='"+OutputRootFileName+"' OPT='NEW'" ])
        acc.addService(ntupleSvc)
        acc.setAppProperty("HistogramPersistency","ROOT")
        
    acc.run()
    
    print ("------------------------5 ")
    
    # Compute the overall time spent to do the analysis
    end = time.time()
    elapsed = end - start
    m, s = divmod(elapsed, 60)
    h, m = divmod(m, 60)
    print ("%d:%02d:%02d" %(h,m,s))
    print( "Ending Processing at : " + bcolors.OKBLUE + datetime.now().strftime('%H:%M:%S') + bcolors.ENDC)
    print( "Duration in overall : " + bcolors.OKBLUE + str("%d:%02d:%02d" %(h,m,s)) + bcolors.ENDC)

        
# ===============================================================
#  __main__
# ===============================================================
if __name__ == '__main__':
    main()


    

    
    
