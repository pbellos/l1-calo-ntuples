 // xAODreader includes
#include "xAODreaderAlg.h"
#include "StoreGate/StoreGateSvc.h"

#include "xAODEventInfo/EventInfo.h"
#include "xAODTrigL1Calo/TriggerTowerContainer.h"
// System include(s):
#include <iomanip>
// ROOT include(s):
#include "TTree.h"
#include "TH1F.h"


xAODreaderAlg :: xAODreaderAlg ( const std::string &name,ISvcLocator *pSvcLocator): AthAlgorithm( name, pSvcLocator ),    
										    m_histSvc( "THistSvc", name )										
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.
}





StatusCode  xAODreaderAlg:: initialize ()
{
  CHECK(m_legacy.initialize() );
  CHECK(m_evtStore.retrieve() );
  CHECK(m_histSvc.retrieve()  );

  m_tree = new TTree("tree","tree");
  

  CHECK( m_histSvc->regTree("/Trigger/m_tree", m_tree) );

  // Event info branches 
  m_EventInfo_runNumber = 0;
  m_tree->Branch("runNumber", &m_EventInfo_runNumber);
  m_EventInfo_eventNumber = 0;
  m_tree->Branch("eventNumber", &m_EventInfo_eventNumber);
  m_EventInfo_timeStamp = 0;
  m_tree->Branch("timeStamp", &m_EventInfo_timeStamp);
  m_EventInfo_timeStampNSOffset = 0;
  m_tree->Branch("timeStampNSOffset", &m_EventInfo_timeStampNSOffset);    
  m_EventInfo_bcid = 0;
  m_tree->Branch("bcid", &m_EventInfo_bcid);   



  // Trigger Tower branches 
  m_tree->Branch("TT_number", &m_CaloLegacy_size);
  m_tree->Branch("TT_E", &m_CaloLegacy_E);
  m_tree->Branch("TT_eta", &m_CaloLegacy_eta);
  m_tree->Branch("TT_phi", &m_CaloLegacy_phi);
  m_tree->Branch("TT_id", &m_CaloLegacy_id);
  m_tree->Branch("TT_layer", &m_CaloLegacy_layer);
  m_tree->Branch("TT_ADCCounts", &m_CaloLegacy_ADCCounts);
  m_tree->Branch("TT_ADCPeakPosition", &m_CaloLegacy_ADCPeakPosition);
  m_tree->Branch("TT_lut_cp", &m_CaloLegacy_lut_cp);
  m_tree->Branch("TT_lut_jep", &m_CaloLegacy_lut_jep);
  m_tree->Branch("TT_CaloCellETAllLayers", &m_CaloCellETByLayer);
  m_tree->Branch("TT_CaloCellETLayer1", &m_CaloCellETLayer1);
  m_tree->Branch("TT_CaloCellETLayer2", &m_CaloCellETLayer2);
  m_tree->Branch("TT_CaloCellETLayer3", &m_CaloCellETLayer3);
  m_tree->Branch("TT_CaloCellETLayer4", &m_CaloCellETLayer4);
  m_tree->Branch("TT_NumberOfLayers",   &m_NumberOflayers);

  


  return StatusCode::SUCCESS;
}


StatusCode  xAODreaderAlg:: execute ()
{
  
  
  ////////////////////////////////
  // EventInfo  
  ////////////////////////////////
  
  const xAOD::EventInfo *evtInfo = nullptr;
  if( m_evtStore->retrieve( evtInfo ).isFailure() ){
    ATH_MSG_WARNING("Unable to retrieve EventInfoContainer: EventInfo");
    return StatusCode::FAILURE;
  }

  m_EventInfo_runNumber   = evtInfo->runNumber();




  SG::ReadHandle<xAOD::TriggerTowerContainer>TTs(m_legacy);
  std::cout << "size Trigger Tower Container " << TTs->size() << std::endl;
  int size =  TTs->size();
    //return StatusCode::SUCCESS;
  m_CaloLegacy_size.push_back(size);
  
  for (auto TT : * TTs){
    m_CaloLegacy_eta.push_back(TT->eta());
    //std::cout << "Trigger Tower Container eta " << TT->eta() << std::endl;
    m_CaloLegacy_phi.push_back(TT->phi()); 
    //std::cout << "Trigger Tower Container phi " << TT->phi() << std::endl;
    m_CaloLegacy_id.push_back(TT->coolId());
    //std::cout << "Trigger Tower Container energy " << TT->e() << std::endl;
    //double E = TT->e()/1e3;
    double E = TT->e();
    m_CaloLegacy_E.push_back(E/cosh( TT->eta()));
  
    m_CaloLegacy_layer.push_back(TT->layer());

    std::vector<unsigned short> adcCounts = TT->adc();
    m_CaloLegacy_ADCCounts.push_back(adcCounts);
    int adcPeakPosition = TT->adcPeak();
    m_CaloLegacy_ADCPeakPosition.push_back(adcPeakPosition);
    
    m_CaloLegacy_lut_cp.push_back( (float)TT->cpET() );
    m_CaloLegacy_lut_jep.push_back( (float)TT->jepET() );
   
    std::vector<float> CaloETLayers = TT->auxdataConst<std::vector<float>> ("CaloCellETByLayer");
    float summedCellEnergyET = 0;
    for (float energy : CaloETLayers) summedCellEnergyET += energy;
    m_CaloCellETByLayer.push_back(summedCellEnergyET); 

    
    //std::cout << "Calo layers size " << CaloETLayers.size() << std::endl;

    if (CaloETLayers.size() >= 4){
      m_CaloCellETLayer1.push_back(CaloETLayers.at(0));
      m_CaloCellETLayer2.push_back(CaloETLayers.at(1));
      m_CaloCellETLayer3.push_back(CaloETLayers.at(2));
      m_CaloCellETLayer4.push_back(CaloETLayers.at(3));
    }
    else {
      m_CaloCellETLayer1.push_back(-1e3);
      m_CaloCellETLayer2.push_back(-1e3);
      m_CaloCellETLayer3.push_back(-1e3);
      m_CaloCellETLayer4.push_back(-1e3);
    }
    m_NumberOflayers.push_back(CaloETLayers.size());
    



  }
  
    
  

  m_tree->Fill();

  m_CaloLegacy_size.clear();
  m_CaloLegacy_E.clear();
  m_CaloLegacy_eta.clear();
  m_CaloLegacy_phi.clear();
  m_CaloLegacy_id.clear();
  m_CaloLegacy_layer.clear();
  m_CaloLegacy_ADCCounts.clear();
  m_CaloLegacy_ADCPeakPosition.clear();
  m_CaloLegacy_lut_cp.clear();
  m_CaloLegacy_lut_jep.clear();
  m_CaloCellETByLayer.clear();
  m_CaloCellETByLayer.clear();
  m_CaloCellETLayer1.clear();
  m_CaloCellETLayer2.clear();
  m_CaloCellETLayer3.clear();
  m_CaloCellETLayer4.clear();
  m_NumberOflayers.clear();


  return StatusCode::SUCCESS;

}
StatusCode  xAODreaderAlg:: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  return StatusCode::SUCCESS;
}

