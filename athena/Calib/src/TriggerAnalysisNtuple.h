// Dear emacs, this is -*- c++ -*-

/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

#ifndef CALIB_TRIGGERANALYSISNTUPLE_H
#define CALIB_TRIGGERANALYSISNTUPLE_H

// STL include(s):
#include <string>
#include <map>
#include <vector>
#include "GaudiKernel/ITHistSvc.h"


// Gaudi/Athena include(s):
#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ToolHandle.h"
#include "GaudiKernel/ServiceHandle.h"
//#include "GaudiKernel/ITHistSvc.h"
#include "GaudiKernel/NTuple.h"
// Trigger include(s):
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TriggerMatchingTool/IMatchingTool.h"
#include "TriggerAnalysisHelper.h"
#include "GaudiKernel/INTupleSvc.h" 
// Forward declaration(s):
class TTree;
class TH1;

/**
 * @brief The TriggerAnalysisTutorial goes together with the trigger
 * part of the ATLAS Software Tutorial
 *
 * @author Joerg Stelzer <stelzer@cern.ch> - DESY
 * @author Ricardo Goncalo <goncalo@cern.ch> - University 
 *
 * Updated August 2016 for ATLAS Trigger for Physics Workshop
 * Some basic trigger analysis code -- see other examples
 * Can we make one common algorithm to put everything together?
 * @author Ryan White <rwhite@cern.ch> - UTFSM
 */
class TriggerAnalysisNtuple : public AthAlgorithm {

public:
   /// Regular algorithm constructor
   TriggerAnalysisNtuple( const std::string& name, ISvcLocator *pSvcLocator );

   ~TriggerAnalysisNtuple();

   /// Function called at the beginning of the job
   virtual StatusCode initialize();
   /// Function called at the end of the job
   virtual StatusCode finalize();
   /// Function called at each event
   virtual StatusCode execute();

private:
   int m_eventNr;
   //int m_eventsPrinted;
  
  std::vector< std::string > m_cfg_chains;
  std::vector< std::string > m_chain_name;

  std::string m_ntpath, m_ntTitle; 
  //Ntuple pointer
  NTuple::Tuple* m_nt;

  NTuple::Item<int>  m_tbp_pass_0;
  NTuple::Item<int>  m_tap_pass_0;
  NTuple::Item<int>  m_tav_pass_0;
  
  NTuple::Item<int>  m_tbp_pass_1;
  NTuple::Item<int>  m_tap_pass_1;
  NTuple::Item<int>  m_tav_pass_1;
  
  NTuple::Item<int>  m_tbp_pass_2;
  NTuple::Item<int>  m_tap_pass_2;
  NTuple::Item<int>  m_tav_pass_2;
  
  //! Helper class for tutorial, provides an additional layer to illustrate TDT functionality
  ToolHandle< Trig::TriggerAnalysisHelper > m_tah; 
   
  ToolHandle< Trig::TrigDecisionTool > m_trigDec{this, "TrigDecisionTool", "", "Handle to the TrigDecisionTool"};

  
  // The THistSvc
  //ServiceHandle< ITHistSvc > m_histSvc;

}; // end of class TriggerAnalysis

#endif // TRIGANALYSISEXAMPLES_TRIGGERANALYSISL_H
