/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

// System include(s):
#include <iomanip>

// ROOT include(s):
#include "TTree.h"
#include "TH1F.h"

// Gaudi/Athena include(s):
#include "AthenaKernel/errorcheck.h"
#include "AthenaKernel/Units.h"


// TrigDecisionTool include(s):
#include "TrigDecisionTool/ChainGroup.h"
#include "TrigDecisionTool/FeatureContainer.h"
#include "TrigDecisionTool/Feature.h"

#include "xAODEventInfo/EventInfo.h"

// Offline EDM include(s):
#include "xAODEgamma/ElectronContainer.h"

// Local include(s):
#include "TriggerAnalysis.h"

using Athena::Units::GeV;

TriggerAnalysis::TriggerAnalysis( const std::string &name,
                                                  ISvcLocator *pSvcLocator)
   : AthAlgorithm( name, pSvcLocator ),
     m_eventNr( 0 ),
     m_h_triggerAccepts( nullptr ),
     m_h_tbp( nullptr ),
     m_h_tap( nullptr ),
     m_h_tav( nullptr ),
     //m_eventsPrinted( 0 ),
     m_trigDec( "Trig::TrigDecisionTool/TrigDecisionTool" ),
     m_matchTool( "Trig::MatchingTool/MatchingTool", this ),
     m_tah( "Trig::TriggerAnalysisHelper/TriggerAnalysisHelper",this ),
     m_histSvc( "THistSvc", name ) {
         
         declareProperty( "TriggerList", m_chain_names, "List of triggers to analyze");
}

TriggerAnalysis::~TriggerAnalysis()
{
}

StatusCode TriggerAnalysis::initialize() {

   // retrieve the tools and services:
   CHECK( m_trigDec.retrieve() );
   CHECK( m_matchTool.retrieve() );
   CHECK( m_histSvc.retrieve() );
   // done
   //
   const int nTrigger = (int) m_chain_names.size();
   m_h_triggerAccepts = new TH1F( "TriggerAccepts", "TriggerAccepts", nTrigger, 0,  nTrigger);
   m_h_tbp = new TH1F( "tbp", "Trigger item Before Prescale", nTrigger, 0, nTrigger );
   m_h_tap = new TH1F( "tap", "Trigger item After Prescale", nTrigger, 0, nTrigger );
   m_h_tav = new TH1F( "tav", "Trigger item After Veto", nTrigger, 0, nTrigger );
   //m_h_l1prescaled = new TH1F( "l1precaled", "Trigger L1 prescaled", nTrigger, 0, nTrigger );

   if ( ! m_chain_names.empty() ){
       for ( int i = 0; i < std::min( (int)m_chain_names.size(), (int)m_h_triggerAccepts->GetNbinsX() ); ++i ) {
           int bin = i+1;
           m_h_triggerAccepts->GetXaxis()->SetBinLabel(bin, m_chain_names[i].c_str());
	   m_h_tbp->GetXaxis()->SetBinLabel(bin, m_chain_names[i].c_str());
	   m_h_tap->GetXaxis()->SetBinLabel(bin, m_chain_names[i].c_str());
	   m_h_tav->GetXaxis()->SetBinLabel(bin, m_chain_names[i].c_str());
	   //m_h_l1prescaled->GetXaxis()->SetBinLabel(bin, m_chain_names[i].c_str());


       }
   }
   CHECK( m_histSvc->regHist( "/Trigger/TriggerAccepts", m_h_triggerAccepts ) );
   CHECK( m_histSvc->regHist( "/Trigger/tbp", m_h_tbp ) );
   CHECK( m_histSvc->regHist( "/Trigger/tap", m_h_tap ) );
   CHECK( m_histSvc->regHist( "/Trigger/tav", m_h_tav ) );
   //CHECK( m_histSvc->regHist( "/Trigger/l1prescaled", m_h_l1prescaled ) );


   m_Trigger = new TTree("trigger","trigger");
  

   CHECK( m_histSvc->regTree("/Trigger/m_Trigger", m_Trigger) );

   // storing a string of trigger menu choice 
   m_Trigger->Branch("TriggerAccepts",&m_trigger_chains);
   m_Trigger->Branch("tbp",&m_tbp_chains);
   m_Trigger->Branch("tap",&m_tap_chains);
   m_Trigger->Branch("tav",&m_tav_chains);

      
   
   ATH_MSG_INFO( "Initialization successful" );



   return StatusCode::SUCCESS;
}

StatusCode TriggerAnalysis::finalize() {

   ATH_MSG_INFO( "STAT Trigger Statistics on " << m_eventNr << " processed events" );


   return StatusCode::SUCCESS;
}

StatusCode TriggerAnalysis::execute() {

   ++m_eventNr;

   ATH_MSG_INFO( "Event Number " << m_eventNr );
   // Get list of configured chains on first event
   // Check L1 chains are configured and push_back on cfg_chains
   if(m_eventNr==1){
       std::vector<std::string> allL1 = m_trigDec->getChainGroup("L1_.*")->getListOfTriggers();
       //Create list of configured chains from input list
       for(const std::string& chain:allL1){
	 ATH_MSG_INFO("Found corresponding chain in list " << chain); 
	 m_cfg_chains.push_back(chain);
       }
       
   }
   
   //std::cout  <<  m_cfg_chains.size() << std::endl;
   for(const auto& chain : m_cfg_chains){
     // What about the L1?
     const unsigned int bits = m_trigDec->isPassedBits(chain);
     bool tbp = bits&TrigDefs::L1_isPassedBeforePrescale;
     bool tap = bits&TrigDefs::L1_isPassedAfterPrescale;
     bool tav = bits&TrigDefs::L1_isPassedAfterVeto;
     // bool l1prescale=tbp && !tap;

     if( m_trigDec->isPassed( chain ) ){
       m_h_triggerAccepts->Fill( chain.c_str(), 1 );
       m_trigger_chains.push_back(chain.c_str());
       //std::cout  << "trigger accepts: " <<chain.c_str()  << std::endl;

     }
   
     if(tbp) {
       m_h_tbp->Fill( chain.c_str(), 1 );
       m_tbp_chains.push_back(chain.c_str());
       //std::cout  << "tbp: " <<chain.c_str()  << std::endl;
     }
     
     if(tap) {
       m_h_tap->Fill( chain.c_str(), 1 );
       m_tap_chains.push_back(chain.c_str());
       //std::cout  << "tap: " <<chain.c_str()  << std::endl;


     }
   
     if(tav) {
       m_h_tav->Fill( chain.c_str(), 1 );
       m_tav_chains.push_back(chain.c_str());
     }
     
     //if(l1prescale) {
     // m_h_l1prescaled->Fill( chain.c_str(), 1 );
     // m_l1prescaled_chains = chain.c_str();
     //}

 }
   
   m_Trigger->Fill();
   m_trigger_chains.clear();
   m_tbp_chains.clear();
   m_tap_chains.clear();
   m_tav_chains.clear();
   
   return StatusCode::SUCCESS;
}

