 // CaloCell includes
#include "CaloCellAlg.h"
#include "StoreGate/StoreGateSvc.h"

#include "xAODEventInfo/EventInfo.h"

#include "Identifier/Identifier.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "CaloIdentifier/CaloIdManager.h"
#include "CaloIdentifier/TileID.h"

// System include(s):
#include <iomanip>
// ROOT include(s):
#include "TTree.h"
#include "TH1F.h"



CaloCellAlg :: CaloCellAlg ( const std::string &name,ISvcLocator *pSvcLocator): AthAlgorithm( name, pSvcLocator ),    
									  m_histSvc( "THistSvc", name )										
{
// Here you put any code for the base initialization of variables,
// e.g. initialize all pointers to 0.  This is also where you
// declare all properties for your algorithm.  Note that things like
// resetting statistics variables or booking histograms should
// rather go into the initialize() function.
}
  



  
  StatusCode  CaloCellAlg:: initialize ()
{
  CHECK( m_clusterContainer.initialize() );
  CHECK( m_histSvc.retrieve()  );
  //CHECK(m_evtStore.retrieve() );
  
  m_calocell = new TTree("calocell","calocell");


  CHECK( m_histSvc->regTree("/Trigger/m_calocell", m_calocell) );
  
  // storing calo cell
   m_calocell->Branch("CaloCell_phi",&m_phi);
   m_calocell->Branch("CaloCell_eta",&m_eta);
   m_calocell->Branch("CaloCell_calE",&m_calE);
   m_calocell->Branch("CaloCell_calM",&m_calM);
   m_calocell->Branch("CaloCell_calPhi",&m_calPhi);
   m_calocell->Branch("CaloCell_calEta",&m_calEta);
   m_calocell->Branch("CaloCell_rawE",&m_rawE);
   m_calocell->Branch("CaloCell_rawM",&m_rawM);
   m_calocell->Branch("CaloCell_rawPhi",&m_rawPhi);
   m_calocell->Branch("CaloCell_rawEta",&m_rawEta);
   m_calocell->Branch("CaloCell_Elayer0",&m_Elayer0);
   m_calocell->Branch("CaloCell_Elayer1",&m_Elayer1);
   m_calocell->Branch("CaloCell_Elayer2",&m_Elayer2);
   m_calocell->Branch("CaloCell_Elayer3",&m_Elayer3);


  return StatusCode::SUCCESS;
}


StatusCode  CaloCellAlg:: execute ()
{
  
  
  SG::ReadHandle<xAOD::CaloClusterContainer> clusterCont{m_clusterContainer};
  if (! clusterCont.isValid()) { ATH_MSG_WARNING("No CaloCluster container found in xAOD"); return StatusCode::FAILURE; }



  std::cout << "size cluster " << clusterCont->size() << std::endl;
  for (auto clus : *clusterCont ){
    //std::cout << "m all layer  " <<clus->calM() << std::endl;	
    //std::cout << "energy all layer  " <<clus->calE() << std::endl;
   
    m_calM.push_back(clus->calM());
    m_calE.push_back(clus->calE()/1e3);
 
    //std::cout << "clus " <<clus->clusterSize()<< std::endl;
    //std::cout << "energy layer 0 " <<clus->energyBE(0) << std::endl;	
    //std::cout << "energy layer 1 " <<clus->energyBE(1) << std::endl;	
    //std::cout << "energy layer 2 " <<clus->energyBE(2) << std::endl;	
    //std::cout << "energy layer 3 " <<clus->energyBE(3) << std::endl;	
    
    m_Elayer0.push_back(clus->energyBE(0)/1e3);
    m_Elayer1.push_back(clus->energyBE(1)/1e3);
    m_Elayer2.push_back(clus->energyBE(2)/1e3);
    m_Elayer3.push_back(clus->energyBE(3)/1e3);



    //std::cout << "eta " <<clus->eta() << std::endl;	
    //std::cout << "phi " <<clus->phi() << std::endl;	

    m_eta.push_back(clus->eta());
    m_phi.push_back(clus->phi());


    //std::cout << "rawE " <<clus->rawE() << std::endl;	
    //std::cout << "rawM " <<clus->rawM() << std::endl;	
    //std::cout << "rawPhi " <<clus->rawPhi() << std::endl;	
    //std::cout << "rawEta " <<clus->rawEta() << std::endl;	

    m_rawE.push_back(clus->rawE()/1e3);
    m_rawM.push_back(clus->rawM());
    m_rawPhi.push_back(clus->rawPhi());
    m_rawEta.push_back(clus->rawEta());
    
    //std::cout << "calPhi " <<clus->calPhi() << std::endl;	
    //std::cout << "calEta " <<clus->calEta() << std::endl;
    
    m_calPhi.push_back(clus->calPhi());
    m_calEta.push_back(clus->calEta());


    
  }


  m_calocell->Fill();

  m_eta.clear();
  m_phi.clear();
  
  m_calE.clear();
  m_calM.clear();
  m_calPhi.clear();
  m_calEta.clear();
  
  m_Elayer0.clear();
  m_Elayer1.clear();
  m_Elayer2.clear();
  m_Elayer3.clear();
  
  m_rawE.clear();
  m_rawM.clear();
  m_rawPhi.clear();
  m_rawEta.clear();

  return StatusCode::SUCCESS;

}
StatusCode  CaloCellAlg:: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  return StatusCode::SUCCESS;
}

