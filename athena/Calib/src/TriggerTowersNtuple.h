#ifndef CALIB_TRIGGERTOWERSNUPLE_H
#define CALIB_TRIGGERTOWERSNTUPLE_H

#include <vector>
#include "TString.h"
#include "AthenaBaseComps/AthAlgTool.h"

#include "xAODTrigL1Calo/TriggerTowerContainer.h"
#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ToolHandle.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/NTuple.h"
#include "StoreGate/StoreGateSvc.h"


class TriggerTowersNtuple: public ::AthAlgorithm {
 public:
    // this is a standard algorithm constructor

  TriggerTowersNtuple( const std::string& name, ISvcLocator* pSvcLocator );


  // these are the functions inherited from Algorithm
  virtual StatusCode initialize ();
  virtual StatusCode execute ();
//virtual StatusCode finalize () override;


 private:

     int m_slices = 0;

     const int m_TT = 7168;

     SG::ReadHandleKey<xAOD::TriggerTowerContainer> m_legacy {this, "TriggerTowerContainer", "xAODTriggerTowers", "Trigger Tower container"};
     ServiceHandle<StoreGateSvc> m_evtStore {this, "EventStore", "StoreGateSvc"};
     // Ntuple path and title
     std::string m_ntpath, m_ntTitle;
     std::string m_ntpath_evt, m_ntTitle_evt;

     //Ntuple pointer
     NTuple::Tuple* m_nt;
     NTuple::Tuple* m_nt2;

     // Event info container information
     NTuple::Item<int>  m_runNumber;
     NTuple::Item<int> m_eventNumber;
     NTuple::Item<int> m_timeStamp;
     NTuple::Item<int> m_timeStampNSOffset;
     NTuple::Item<int> m_bcid;

     // Trigger tower container information
     NTuple::Array<float>  m_eta;
     NTuple::Array<float>  m_phi;
     NTuple::Array<uint32_t>  m_coolId;
     NTuple::Array<int>  m_layer;
     NTuple::Matrix<int>  m_adcCounts;
     NTuple::Array<int>  m_adcPeak;
     NTuple::Array<int> m_cpET;
     NTuple::Array<int> m_jepET;
     NTuple::Array<float> m_cellET;
     NTuple::Array <unsigned long long> m_IEvent;



};

#endif //> !MYPACKAGE_MYPACKAGEALG_H
