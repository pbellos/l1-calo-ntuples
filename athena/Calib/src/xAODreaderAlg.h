#ifndef CALIB_XAODREADERALG_H
#define CALIB_XAODREADER_H 

#include <vector>
#include "TString.h"
#include "AthenaBaseComps/AthAlgTool.h"

//#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include "xAODTrigL1Calo/TriggerTowerContainer.h"
//#include "AthenaBaseComps/AthAlgorithm.h"
// Gaudi/Athena include(s):
#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ToolHandle.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ITHistSvc.h"
#include "StoreGate/StoreGateSvc.h"

//Example ROOT Includes
#include "TTree.h"

class xAODreaderAlg: public ::AthAlgorithm { 
 public: 
    // this is a standard algorithm constructor

  xAODreaderAlg( const std::string& name, ISvcLocator* pSvcLocator );


  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;


 private: 
  TTree* m_tree = nullptr;
  ServiceHandle< ITHistSvc > m_histSvc;
  SG::ReadHandleKey<xAOD::TriggerTowerContainer> m_legacy {this, "TriggerTowerContainer", "xAODTriggerTowers", "Trigger Tower container"};
  ServiceHandle<StoreGateSvc> m_evtStore {this, "EventStore", "StoreGateSvc"};
  
  // Event info container information

  int m_EventInfo_runNumber;
  int m_EventInfo_eventNumber;
  int m_EventInfo_timeStamp;
  int m_EventInfo_timeStampNSOffset;
  int m_EventInfo_bcid;


  // Trigger Tower container information 
  std::vector<float>  m_CaloLegacy_E;
  std::vector<float>  m_CaloLegacy_eta;
  std::vector<float>  m_CaloLegacy_phi;
  std::vector<int>    m_CaloLegacy_id;
  std::vector<int>    m_CaloLegacy_size;
  std::vector<float>  m_CaloLegacy_layer;
  std::vector< std::vector<unsigned short> > m_CaloLegacy_ADCCounts;
  std::vector<int> m_CaloLegacy_ADCPeakPosition;
  std::vector<float> m_CaloLegacy_lut_cp;
  std::vector<float> m_CaloLegacy_lut_jep;
  std::vector<float>   m_CaloCellETByLayer;
  std::vector<float>   m_CaloCellETLayer1;
  std::vector<float>   m_CaloCellETLayer2;
  std::vector<float>   m_CaloCellETLayer3;
  std::vector<float>   m_CaloCellETLayer4;
  std::vector<int>  m_NumberOflayers;

}; 

#endif //> !MYPACKAGE_MYPACKAGEALG_H
