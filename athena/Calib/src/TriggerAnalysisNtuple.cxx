/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

// System include(s):
#include <iomanip>

// ROOT include(s):
#include "TTree.h"
#include "TH1F.h"

// Gaudi/Athena include(s):
#include "AthenaKernel/errorcheck.h"
#include "AthenaKernel/Units.h"



// TrigDecisionTool include(s):
#include "TrigDecisionTool/ChainGroup.h"
#include "TrigDecisionTool/FeatureContainer.h"
#include "TrigDecisionTool/Feature.h"

#include "xAODEventInfo/EventInfo.h"

// Offline EDM include(s):
#include "xAODEgamma/ElectronContainer.h"

// Local include(s):
#include "TriggerAnalysisNtuple.h"

using Athena::Units::GeV;

TriggerAnalysisNtuple::TriggerAnalysisNtuple( const std::string &name,
                                                  ISvcLocator *pSvcLocator)
   : AthAlgorithm( name, pSvcLocator ),
     m_eventNr( 0 ),
     m_tah( "Trig::TriggerAnalysisHelper/TriggerAnalysisHelper",this )
{
         declareProperty( "TrigDecisionTool", m_trigDec, "TriggerDecision");
         declareProperty( "TriggerList", m_chain_name, "List of triggers to analyze");
	 m_ntTitle = "TriggerMenu";     
	 m_ntpath  = "/NTUPLES/FILE1/TRIGGERMENU";    
}

TriggerAnalysisNtuple::~TriggerAnalysisNtuple()
{
}

StatusCode TriggerAnalysisNtuple::initialize() {

   // retrieve the tools and services:
   CHECK( m_trigDec.retrieve() );
   
   
   const int nTrigger = (int) m_chain_name.size();
   ATH_MSG_DEBUG( "nTrigger size: " << nTrigger);
  
   if (nTrigger > 3) {
     ATH_MSG_FATAL( "Trigger chains more than 3, please reduce it to a len ==3" );
     return StatusCode::FAILURE; 
   }
   


    // Initializating ntuple
   size_t i=m_ntpath.rfind('/');
   if (i==std::string::npos) {
     ATH_MSG_ERROR( "Expected at least on '/' in path " << m_ntpath );
     return StatusCode::FAILURE;
   }
   std::string basepath(m_ntpath.begin(),m_ntpath.begin()+i);
  

   NTupleFilePtr file1(ntupleSvc(),basepath);
   if (!file1){
     ATH_MSG_ERROR( "Could not get NTupleFilePtr with path " << basepath << " failed" );
     return StatusCode::FAILURE;
   }
   NTuplePtr nt(ntupleSvc(),m_ntpath);
   if (!nt) {
     nt=ntupleSvc()->book(m_ntpath,CLID_ColumnWiseTuple,m_ntTitle);
   }
   if (!nt){
     ATH_MSG_ERROR( "Booking of NTuple at "<< m_ntpath << " and name " << m_ntTitle << " failed" );
     return StatusCode::FAILURE; 
   }

   m_nt=nt;

   StatusCode sc;
  
      
   // first chain
   sc = m_nt->addItem("tbp_"+m_chain_name[0],m_tbp_pass_0);
   if (sc!=StatusCode::SUCCESS) {
     ATH_MSG_ERROR( "addItem --> tbp_" << m_chain_name[0] <<  " failed" );
     return sc;
   }
   
   
   sc = m_nt->addItem("tap_"+m_chain_name[0],m_tap_pass_0);
   if (sc!=StatusCode::SUCCESS) {
     ATH_MSG_ERROR( "addItem --> tap_" << m_chain_name[0] <<  " failed" );
     return sc;
   }
   
   
   sc = m_nt->addItem("tav_"+m_chain_name[0],m_tav_pass_0);
   if (sc!=StatusCode::SUCCESS) {
     ATH_MSG_ERROR( "addItem --> tav_" << m_chain_name <<  " failed" );
     return sc;
   }
   

   // second chain (if there is any)
   if (nTrigger > 1) { 
     sc = m_nt->addItem("tbp_"+m_chain_name[1],m_tbp_pass_1);
     if (sc!=StatusCode::SUCCESS) {
       ATH_MSG_ERROR( "addItem --> tbp_" << m_chain_name[1] <<  " failed" );
       return sc;
     }
   
   
     sc = m_nt->addItem("tap_"+m_chain_name[1],m_tap_pass_1);
     if (sc!=StatusCode::SUCCESS) {
       ATH_MSG_ERROR( "addItem --> tap_" << m_chain_name[1] <<  " failed" );
       return sc;
     }
   
   
     sc = m_nt->addItem("tav_"+m_chain_name[1],m_tav_pass_1);
     if (sc!=StatusCode::SUCCESS) {
       ATH_MSG_ERROR( "addItem --> tav_" << m_chain_name[1] <<  " failed" );
       return sc;
     }
   }

   
   // third  chain (if there is any)
   if (nTrigger > 2) { 
     sc = m_nt->addItem("tbp_"+m_chain_name[2],m_tbp_pass_2);
     if (sc!=StatusCode::SUCCESS) {
       ATH_MSG_ERROR( "addItem --> tbp_" << m_chain_name[2] <<  " failed" );
       return sc;
     }
     
     
     sc = m_nt->addItem("tap_"+m_chain_name[2],m_tap_pass_2);
     if (sc!=StatusCode::SUCCESS) {
       ATH_MSG_ERROR( "addItem --> tap_" << m_chain_name[2] <<  " failed" );
       return sc;
     }
     
     
     sc = m_nt->addItem("tav_"+m_chain_name[2],m_tav_pass_2);
     if (sc!=StatusCode::SUCCESS) {
       ATH_MSG_ERROR( "addItem --> tav_" << m_chain_name[2] <<  " failed" );
       return sc;
     }
   }
   

   

   ATH_MSG_INFO( "Initialization successful" );



   return StatusCode::SUCCESS;
}

StatusCode TriggerAnalysisNtuple::finalize() {

   ATH_MSG_INFO( "STAT Trigger Statistics on " << m_eventNr << " processed events" );


   return StatusCode::SUCCESS;
}

StatusCode TriggerAnalysisNtuple::execute() {


   ATH_MSG_INFO( "Trigger Menu Ntuple in execute" ); 
   ++m_eventNr;

   ATH_MSG_INFO( "Event Number " << m_eventNr );
   // Get list of configured chains on first event
   // Check L1 chains are configured and push_back on cfg_chains
   if(m_eventNr==1){
       std::vector<std::string> allL1 = m_trigDec->getChainGroup("L1_.*")->getListOfTriggers();
       //Create list of configured chains from input list
       for(const std::string& chain:allL1){
	 ATH_MSG_INFO("Found corresponding chain in list " << chain); 
	 m_cfg_chains.push_back(chain);
       }
       
   }
   
   
   m_tap_pass_0 = 0;
   m_tav_pass_0 = 0;
   m_tbp_pass_0 = 0;

   m_tap_pass_1 = 0;
   m_tav_pass_1 = 0;
   m_tbp_pass_1 = 0;

   m_tap_pass_2 = 0;
   m_tav_pass_2 = 0;
   m_tbp_pass_2 = 0;
   
   //for(const auto& chain : m_cfg_chains){
   // Using a for loop with index
   for(std::size_t i = 0; i < m_chain_name.size(); ++i) {
     // What about the L1?
     ATH_MSG_DEBUG("chain in list " << m_chain_name[i] << "i : " << i);
     const unsigned int bits = m_trigDec->isPassedBits(m_chain_name[i]);
     bool tbp = bits&TrigDefs::L1_isPassedBeforePrescale;
     bool tap = bits&TrigDefs::L1_isPassedAfterPrescale;
     bool tav = bits&TrigDefs::L1_isPassedAfterVeto;
     // bool l1prescale=tbp && !tap;
     if (i==0) {
       if(tbp) {
	 m_tbp_pass_0 = 1;
       }
       if(tap) {
	 m_tap_pass_0 = 1;
	  
       }
       if(tav) {
	 m_tav_pass_0 = 1;
	  
       }
     }
     else if  (i==1){
       if(tbp) {
	 m_tbp_pass_1 = 1;
       }
       if(tap) {
	 m_tap_pass_1 = 1;
	  
       }
       if(tav) {
	 m_tav_pass_1 = 1;
       }
       
     }
     
     else if  (i==2){
       if(tbp) {
	 m_tbp_pass_2 = 1;
       }
       if(tap) {
	 m_tap_pass_2 = 1;
	  
       }
       if(tav) {
	 m_tav_pass_2 = 1;
       }
       
     }
   }
       
  
      
   StatusCode sc   = ntupleSvc()->writeRecord(m_nt);
   if (sc != StatusCode::SUCCESS) {
     ATH_MSG_ERROR( "writeRecord failed" );
     return sc;}
  
   
   return StatusCode::SUCCESS;
}

