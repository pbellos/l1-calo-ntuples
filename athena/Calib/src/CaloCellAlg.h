#ifndef CALIB_CALOCELLALG_H
#define CALIB_CALOCELLALG_H 

#include <vector>
#include "TString.h"
#include "AthenaBaseComps/AthAlgTool.h"

#include "Identifier/Identifier.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "CaloIdentifier/CaloIdManager.h"
#include "CaloIdentifier/TileID.h"




#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ToolHandle.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ITHistSvc.h"
#include "StoreGate/StoreGateSvc.h"

//Example ROOT Includes
#include "TTree.h"

class CaloCellAlg: public ::AthAlgorithm { 
 public: 
    // this is a standard algorithm constructor

  CaloCellAlg( const std::string& name, ISvcLocator* pSvcLocator );


  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;


 private: 
  TTree* m_calocell = nullptr;
  ServiceHandle< ITHistSvc > m_histSvc;
 
  SG::ReadHandleKey<xAOD::CaloClusterContainer> m_clusterContainer{ this, "CaloClusterContainer", "CaloCalTopoClusters" };
  std::vector<float>  m_phi;
  std::vector<float>  m_eta;
  
  std::vector<float>  m_calM;
  std::vector<float>  m_calE;
  std::vector<float>  m_calPhi;
  std::vector<float>  m_calEta;
  
  std::vector<float> m_Elayer0;
  std::vector<float> m_Elayer1;
  std::vector<float> m_Elayer2;
  std::vector<float> m_Elayer3;
  
  std::vector<float> m_rawE;
  std::vector<float> m_rawM;
  std::vector<float> m_rawPhi;
  std::vector<float> m_rawEta;
  


}; 

#endif //> !MYPACKAGE_MYPACKAGEALG_H
