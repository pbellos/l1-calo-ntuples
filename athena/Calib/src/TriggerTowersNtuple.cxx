
/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/




 // TriggerTowerNtuple includes
#include "TriggerTowersNtuple.h"
#include "StoreGate/StoreGateSvc.h"

#include "xAODEventInfo/EventInfo.h"
#include "xAODTrigL1Calo/TriggerTowerContainer.h"
// System include(s):
#include <iomanip>
// ROOT include(s):
#include "TH1F.h"



TriggerTowersNtuple :: TriggerTowersNtuple ( const std::string &name,ISvcLocator *pSvcLocator): AthAlgorithm( name, pSvcLocator ),
												m_slices(15)
{
  declareProperty("slices",m_slices);
  m_ntTitle = "TriggerTowers";
  m_ntpath  = "/NTUPLES/FILE1/TRIGGERTOWERS";
  m_ntTitle_evt = "EventInfo";
  m_ntpath_evt  = "/NTUPLES/FILE1/EVENTINFO";
}





StatusCode  TriggerTowersNtuple:: initialize ()
{
  CHECK(m_legacy.initialize() );
  CHECK(m_evtStore.retrieve() );



  // Initializating ntuple
  size_t i=m_ntpath.rfind('/');
  if (i==std::string::npos) {
    ATH_MSG_ERROR( "Expected at least on '/' in path " << m_ntpath );
    return StatusCode::FAILURE;
  }
  std::string basepath(m_ntpath.begin(),m_ntpath.begin()+i);


  NTupleFilePtr file1(ntupleSvc(),basepath);
  if (!file1){
    ATH_MSG_ERROR( "Could not get NTupleFilePtr with path " << basepath << " failed" );
    return StatusCode::FAILURE;
  }
  NTuplePtr nt(ntupleSvc(),m_ntpath);
  if (!nt) {
    nt=ntupleSvc()->book(m_ntpath,CLID_ColumnWiseTuple,m_ntTitle);
  }
  if (!nt){
    ATH_MSG_ERROR( "Booking of NTuple at "<< m_ntpath << " and name " << m_ntTitle << " failed" );
    return StatusCode::FAILURE;
  }

  m_nt=nt;


  // Initializating ntuple
  size_t j=m_ntpath_evt.rfind('/');
  if (j==std::string::npos) {
    ATH_MSG_ERROR( "Expected at least on '/' in path " << m_ntpath_evt );
    return StatusCode::FAILURE;
  }
  std::string basepath_evt(m_ntpath_evt.begin(),m_ntpath_evt.begin()+i);


  NTupleFilePtr file2(ntupleSvc(),basepath_evt);
  if (!file2){
    ATH_MSG_ERROR( "Could not get NTupleFilePtr with path " << basepath_evt << " failed" );
    return StatusCode::FAILURE;
  }
  NTuplePtr nt2(ntupleSvc(),m_ntpath_evt);
  if (!nt2) {
    nt2=ntupleSvc()->book(m_ntpath_evt,CLID_ColumnWiseTuple,m_ntTitle_evt);
  }
  if (!nt2){
    ATH_MSG_ERROR( "Booking of NTuple at "<< m_ntpath_evt << " and name " << m_ntTitle_evt << " failed" );
    return StatusCode::FAILURE;
  }

  m_nt2=nt2;


  StatusCode sc;



  // Event info branches
  sc = m_nt2->addItem("runNumber",m_runNumber);
  if (sc!=StatusCode::SUCCESS) {
    ATH_MSG_ERROR( "addItem 'm_runNumber' failed" );
    return sc;
  }


  sc = m_nt2->addItem("eventNumber",m_eventNumber);
  if (sc!=StatusCode::SUCCESS) {
    ATH_MSG_ERROR( "addItem 'm_eventNumber' failed" );
    return sc;
  }

  sc = m_nt2->addItem("timeStamp",m_timeStamp);
  if (sc!=StatusCode::SUCCESS) {
    ATH_MSG_ERROR( "addItem 'm_timeStamp' failed" );
    return sc;
  }

  sc = m_nt2->addItem("timeStampNSOffset",m_timeStampNSOffset);
  if (sc!=StatusCode::SUCCESS) {
    ATH_MSG_ERROR( "addItem 'm_timeStampNSOffset' failed" );
    return sc;
  }

  sc = m_nt2->addItem("bcid",m_bcid);
  if (sc!=StatusCode::SUCCESS) {
    ATH_MSG_ERROR( "addItem 'm_bcid' failed" );
    return sc;
  }


  // Trigger tower branches

  sc=m_nt->addItem("IEvent",m_TT,m_IEvent,0,3000);
  if (sc!=StatusCode::SUCCESS) {
    ATH_MSG_ERROR( "addItem 'IEvent' failed" );
    return sc;
  }


  sc = m_nt->addItem("coolId",m_TT,m_coolId);
  if (sc!=StatusCode::SUCCESS) {
    ATH_MSG_ERROR( "addItem 'm_coolId' failed" );
    return sc;
  }


  sc = m_nt->addItem("phi",m_TT,m_phi);
  if (sc!=StatusCode::SUCCESS) {
    ATH_MSG_ERROR( "addItem 'm_phi' failed" );
    return sc;
  }

  sc = m_nt->addItem("eta",m_TT,m_eta);
  if (sc!=StatusCode::SUCCESS) {
    ATH_MSG_ERROR( "addItem 'm_eta' failed" );
    return sc;
  }


  // get layer (0=EM, 1=Had, 2=FCAL23)
  sc = m_nt->addItem("layer",m_TT,m_layer);
  if (sc!=StatusCode::SUCCESS) {
    ATH_MSG_ERROR( "addItem 'm_layer' failed" );
    return sc;
  }

  sc = m_nt->addItem("adcCounts",m_TT,m_slices,m_adcCounts);
  if (sc!=StatusCode::SUCCESS) {
    ATH_MSG_ERROR( "addItem 'm_adcCounts' failed" );
    return sc;
  }


  sc = m_nt->addItem("adcPeak",m_TT,m_adcPeak);
  if (sc!=StatusCode::SUCCESS) {
    ATH_MSG_ERROR( "addItem 'm_adcPeak' failed" );
    return sc;
  }


  sc = m_nt->addItem("cpET", m_TT,m_cpET);
  if (sc!=StatusCode::SUCCESS) {
    ATH_MSG_ERROR( "addItem 'm_cpET' failed" );
    return sc;}

  sc = m_nt->addItem("jepET", m_TT,m_jepET);
  if (sc!=StatusCode::SUCCESS) {
    ATH_MSG_ERROR( "addItem 'm_jepET' failed" );
    return sc;}

  sc = m_nt->addItem("cellET", m_TT,m_cellET);
  if (sc!=StatusCode::SUCCESS) {
    ATH_MSG_ERROR( "addItem 'm_cellET' failed" );
    return sc;}



  return StatusCode::SUCCESS;
}


StatusCode  TriggerTowersNtuple:: execute ()
{

   ATH_MSG_INFO( "Trigger Towers Ntuple in execute" );

  //m_event ++;

  ////////////////////////////////
  // EventInfo
  ////////////////////////////////

   const xAOD::EventInfo *evtInfo = nullptr;
   if( m_evtStore->retrieve( evtInfo ).isFailure() ){
     ATH_MSG_WARNING("Unable to retrieve EventInfoContainer: EventInfo");
     return StatusCode::FAILURE;
   }

   m_runNumber   = evtInfo->runNumber();
   m_eventNumber = evtInfo->eventNumber();
   m_timeStamp   = evtInfo->timeStamp();
   m_timeStampNSOffset = evtInfo->timeStampNSOffset();
   m_bcid        = evtInfo->bcid();



  ////////////////////////////////
  // Trigger Towers
  ////////////////////////////////

  SG::ReadHandle<xAOD::TriggerTowerContainer>TTs(m_legacy);

  int N=-1;

  for (auto TT : * TTs){

    N++;

    // Event identification
    m_IEvent[N]=m_eventNumber;

    m_eta[N] = TT->eta();
    m_phi[N] = TT->phi();
    m_coolId[N] = TT->coolId();


    std::vector<unsigned short> adcCounts = TT->adc();
    //std::cout << "size ADC " << adcCounts.size() << std::endl;
    for(unsigned i =	0; i<adcCounts.size();++i){
      m_adcCounts[N][i] = TT->adc().at(i);
      //if (TT->adc().at(i) > 1 )
    }


    m_adcPeak[N] = TT->adcPeak();
    m_cpET[N] = TT->cpET();
    m_jepET[N] = TT->jepET();




    std::vector<float> CaloETLayers = TT->auxdataConst<std::vector<float>> ("CaloCellETByLayer");
    //float  m_cellET = 0;
    for (float energy : CaloETLayers) m_cellET[N] += energy;







  }


   StatusCode sc   = ntupleSvc()->writeRecord(m_nt);
    if (sc != StatusCode::SUCCESS) {
      ATH_MSG_ERROR( "writeRecord failed" );
      return sc;}



  StatusCode sc2   = ntupleSvc()->writeRecord(m_nt2);
  if (sc2 != StatusCode::SUCCESS) {
    ATH_MSG_ERROR( "writeRecord failed" );
    return sc2;}



  return StatusCode::SUCCESS;

}






