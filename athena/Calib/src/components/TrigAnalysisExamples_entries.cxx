#include "../TriggerAnalysisHelper.h"
#include "../xAODreaderAlg.h"
#include "../TriggerAnalysis.h"
#include "../CaloCellAlg.h"
#include "../TriggerTowersNtuple.h"
#include "../TriggerAnalysisNtuple.h"


DECLARE_COMPONENT( Trig::TriggerAnalysisHelper )
DECLARE_COMPONENT( xAODreaderAlg )
DECLARE_COMPONENT( TriggerAnalysis )
DECLARE_COMPONENT( CaloCellAlg )
DECLARE_COMPONENT( TriggerTowersNtuple )
DECLARE_COMPONENT( TriggerAnalysisNtuple )

